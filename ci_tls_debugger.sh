#!/bin/sh

# never fail
set +e +o pipefail

debugger_version=0.0.1

is_container() {
  [ "$CI_DISPOSABLE_ENVIRONMENT" = "true" ]
}

detect_environment() {
  echo GitLab CI TLS debugger version: "$debugger_version"
  echo GitLab server version: "$CI_SERVER_VERSION"
  echo Disposable environment: "${CI_DISPOSABLE_ENVIRONMENT:-false}"
  echo Shared environment: "${CI_SHARED_ENVIRONMENT:-false}"
  echo Current directory: "$(pwd)"
}

detect_git_setup() {
  git version
  echo git global configuration
  git config --global -l
  echo "############################"
  echo git local configuration
  git config -l
  echo "############################"
  git remote -v
}

test_https() {
  curl -i -v --head "$CI_REPOSITORY_URL"
}

dump_ca() {
  if [ -z "$CI_SERVER_TLS_CA_FILE" ] ; then
    echo No CI_SERVER_TLS_CA_FILE variable
  else
    cat "$CI_SERVER_TLS_CA_FILE"
  fi
}

setup() {
  is_container && apk add -U git curl ca-certificates
}

main() {
  if [ -z "$CI_SERVER" ]; then
    echo Not running in CI - debug mode
    GIT_CURL_VERBOSE=1 git fetch -v
    detect_environment
    detect_git_setup
    dump_ca
    CI_REPOSITORY_URL="$(git remote get-url origin)" test_https
  fi

  # main will do nothing in CI
}

main
